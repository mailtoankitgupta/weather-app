import { Card, HStack, Stack, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import WeatherAPI from "../api/weatherApi";
import { Temperature, WeatherIcon } from "./CurrentWeather";

function WeatherCard({ city }: any) {
  const [icon, setIcon] = useState(false);
  const [temp, setTemp] = useState(0);
  const [feelsLike, setFeelsLike] = useState(0);

  useEffect(() => {
    WeatherAPI.getWeatherData(city).then((data: any) => {
      if (!data) return;
      setIcon(data?.weather?.length > 0 ? data?.weather[0]?.icon : false);
      setTemp(Math.round(data?.main?.temp));
      setFeelsLike(Math.round(data?.main?.feels_like));
    });
    const intervalId = setInterval(async () => {}, 60000); // Update weather every 1 minute
    return () => clearInterval(intervalId);
  }, [city]);

  return (
    <Card padding={4}>
      <Stack>
        <Text as="h2">{city}</Text>
        <HStack>
          {icon && <WeatherIcon icon={icon} boxSize={"20"} />}
          <Temperature current={temp} feelsLike={feelsLike} size={"3xl"} />
        </HStack>
      </Stack>
    </Card>
  );
}

export default WeatherCard;
