// 1. Import
"use client";

import { Center, HStack, Icon, Image, Stack, Text } from "@chakra-ui/react";
import { CiDroplet } from "react-icons/ci";
import { TiWeatherWindy } from "react-icons/ti";

export function Temperature({
  current,
  feelsLike,
  size,
}: {
  current: number;
  feelsLike: number;
  size?: string;
}) {
  return (
    <Stack spacing={0}>
      <Text fontSize={size ? size : "6xl"} fontWeight={"bold"} align={"center"}>
        {current}°
      </Text>
      <Text fontSize={size ? "sm" : "md"} color={"gray.400"} align={"center"}>
        Feels like {feelsLike}°
      </Text>
    </Stack>
  );
}

export function WeatherIcon({
  icon,
  boxSize,
}: {
  icon: any;
  boxSize?: string;
}) {
  const imageUrl = `https://openweathermap.org/img/wn/${icon}@4x.png`;
  return (
    <Image
      src={imageUrl}
      alt="Weather Icon"
      boxSize={boxSize ? boxSize : "120"}
    />
  );
}

function Stat({ icon, value, unit }: { icon: any; value: any; unit: any }) {
  return (
    <HStack>
      <Icon as={icon} boxSize={5} color="gray.500" />
      <Text fontSize="xl" color="gray.400">
        {value}
      </Text>
      <Text fontSize="sm" color="gray.400">
        {unit}
      </Text>
    </HStack>
  );
}

export function CurrentWeather({ weatherData }: { weatherData: any }) {
  const temp = Math.round(weatherData?.main?.temp);
  const feelsLike = Math.round(weatherData?.main?.feels_like);
  const windSpeed = Math.round(weatherData?.wind?.speed);

  const showIcon = weatherData?.weather?.length > 0;

  return (
    <Center>
      <HStack spacing={12}>
        {showIcon && <WeatherIcon icon={weatherData?.weather[0]?.icon} />}
        <Temperature current={temp} feelsLike={feelsLike} />
        <Stack>
          <Stat icon={TiWeatherWindy} value={windSpeed} unit={"m/s"} />
          <Stat
            icon={CiDroplet}
            value={weatherData?.main?.humidity}
            unit={"%"}
          />
        </Stack>
      </HStack>
    </Center>
  );
}
