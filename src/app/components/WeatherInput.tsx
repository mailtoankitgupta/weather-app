import { HStack, Input, Text } from "@chakra-ui/react";
import { useEffect, useState } from "react";

export function WeatherInput({ setCity, weatherData }: any) {
  const [weatherDesc, setWeatherDesc] = useState("");
  let inputTimeoutId: any = undefined;

  const handleChange = (e: any) => {
    const newCity = e.target.value;

    // Delay in milliseconds before updating the state
    const delay = 500; // 1 second delay

    // Clear any existing timeout
    if (typeof inputTimeoutId !== "undefined") {
      clearTimeout(inputTimeoutId);
    }

    // Set a new timeout to update the state after the delay
    inputTimeoutId = setTimeout(() => {
      console.log("Updating city after delay:", newCity);
      setCity(newCity);
    }, delay);
  };

  useEffect(() => {
    if (weatherData?.weather?.length > 0) {
      setWeatherDesc(weatherData.weather[0].description);
    }
  });

  return (
    <HStack>
      <Text whiteSpace={"nowrap"} color={"gray.400"}>
        Right now in
      </Text>
      <Input
        width={"auto"}
        size="lg"
        flex={1}
        fontWeight={"bold"}
        borderRadius="lg"
        padding="0.5rem"
        variant="flushed"
        placeholder="City, Country"
        _placeholder={{ color: "gray.400" }}
        // @ts-ignore
        onChange={handleChange}
      />
      <Text whiteSpace={"nowrap"} color={"gray.400"}>
        {weatherDesc && `, it's ${weatherDesc}`}
      </Text>
    </HStack>
  );
}
