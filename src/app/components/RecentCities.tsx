import { Box, Flex, Text } from "@chakra-ui/react";
import WeatherCard from "./WeatherCard"; // Assume WeatherCard is a component for displaying weather information

function RecentLocations({ cities, ...props }: any) {
  return (
    <Box {...props}>
      <Text as="h2" color={"gray.400"}>
        Recent Locations
      </Text>
      <Flex marginTop={5} gap={5}>
        {cities.map((city: any) => (
          // @ts-ignore
          <WeatherCard key={city} city={city} />
        ))}
      </Flex>
    </Box>
  );
}

export default RecentLocations;
