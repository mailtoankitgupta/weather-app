class WeatherAPI {
  async getWeatherData(city: string) {
    try {
      const response = await fetch("https://weather-app-python2.vercel.app/weather/" + city);
      const data = await response.json();
      console.log(data);
      if (data.cod !== 200) {
        console.error("Error fetching weather data:", data);
        return null;
      }
      return data;
    } catch (error) {
      console.error("Error fetching weather data:", error);
      return null;
    }
  }
}

export default new WeatherAPI();
