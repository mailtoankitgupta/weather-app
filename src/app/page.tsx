"use client";

import { Box, Center, Stack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import WeatherAPI from "./api/weatherApi";
import { CurrentWeather } from "./components/CurrentWeather";
import RecentLocations from "./components/RecentCities";
import { WeatherInput } from "./components/WeatherInput";

export default function Home() {
  const [city, setCity] = useState("");
  const [cities, setCities]: any = useState([]);
  const [weatherData, setWeatherData] = useState(null);

  useEffect(() => {
    if (city === "") return;
    WeatherAPI.getWeatherData(city).then((data: any) => {
      if (!data || cities.includes(city)) {
        return;
      }
      setWeatherData(data);
      const newCities = [city, ...cities];
      // Keep top-3
      if (newCities.length > 3) {
        newCities.pop();
      }
      setCities(newCities);
    });
  }, [city]);

  return (
    <Box backgroundColor={"white"}>
      <Center h="100vh">
        <Stack>
          <WeatherInput setCity={setCity} weatherData={weatherData} />
          {weatherData && <CurrentWeather weatherData={weatherData} />}
          {cities.length > 0 && (
            <RecentLocations cities={cities} marginTop={20} />
          )}
        </Stack>
      </Center>
    </Box>
  );
}
